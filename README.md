# AWS Route53 Terraform repository

This repository contains my primary domain DNS records.
The DNSSEC configuration is not included (yet?).

## How to `terraform init`

 1) Go to [Terraform states](https://gitlab.com/pdostal/dns/-/terraform).
 2) Copy the `terraform init` command but replace `<YOUR-ACCESS-TOKEN>`.
 3) Enjoy local `terraform`!


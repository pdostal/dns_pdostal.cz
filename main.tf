terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }

  backend "http" {
  }
}
provider "aws" {
  region  = "eu-central-1"
  profile = "terraform"
}


# pdostal.cz

resource "aws_route53_zone" "pdostal_cz" {
  name = "pdostal.cz"
}

resource "aws_route53_record" "pdostal_cz_A" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "pdostal.cz."
  type    = "A"
  ttl     = 300
  records = [
    "62.171.184.19",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_AAAA" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "pdostal.cz."
  type    = "AAAA"
  ttl     = 300
  records = [
    "2a02:c207:2046:2263::1",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_MX" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "pdostal.cz."
  type    = "MX"
  ttl     = 300
  records = [
    "10 mx01.mail.icloud.com",
    "20 mx02.mail.icloud.com",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_TXT" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "pdostal.cz."
  type    = "TXT"
  ttl     = 300
  records = [
    "apple-domain=QBW8ahWzj1Icr3kF",
    "v=spf1 include:icloud.com ~all",
    "keybase-site-verification=djhuxmwVR_UMrQ6zCFpB2oGgMZCZvsFbcRTA9OG5_30",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_dmarc_TXT" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "_dmarc.pdostal.cz."
  type    = "TXT"
  ttl     = 300
  records = [
    "v=DMARC1; p=quarantine; rua=mailto:dmarc-reports@pdostal.cz",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_adsp_domainkey_TXT" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "_adsp._domainkey.pdostal.cz."
  type    = "TXT"
  ttl     = 300
  records = [
    "dkim=unknown",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_mesmtp_domainkey_TXT" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "mesmtp._domainkey.pdostal.cz."
  type    = "TXT"
  ttl     = 300
  records = [
    "v=DKIM1; k=rsa;p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDA0qq9ooUO1DJidItzRzAeQ0a4EB+XGJ26cmh5J20riEtU5V+krPT0RlKXKKsix01lz8RJxr7jYtnKnjRrI51OgzyYmpowwC7EiLcT4yI1xj9qt56w94ClR/DjuVMjdRHS0VFaFuc8nW0wW4QpcQjRo7TI5ADe4JpT2F527hOEzQIDAQAB",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_sig1_domainkey_CNAME" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "sig1._domainkey.pdostal.cz."
  type    = "CNAME"
  ttl     = 300
  records = [
    "sig1.dkim.pdostal.cz.at.icloudmailadmin.com",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_gitlab_pages_verification_code_TXT" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "_gitlab-pages-verification-code.pdostal.cz."
  type    = "TXT"
  ttl     = 300
  records = [
    "gitlab-pages-verification-code=c3a7217857c357ac5bb5eb2927ae2374",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_client_smtp_SRV" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "_client._smtp.pdostal.cz."
  type    = "SRV"
  ttl     = 300
  records = [
    "1 1 1 pdostal.cz",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_caldavs_tcp_SRV" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "_caldavs._tcp.pdostal.cz."
  type    = "SRV"
  ttl     = 300
  records = [
    "0 1 443 caldav.messagingengine.com",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_carddavs_tcp_SRV" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "_carddavs._tcp.pdostal.cz."
  type    = "SRV"
  ttl     = 300
  records = [
    "0 1 443 carddav.messagingengine.com",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_imaps_tcp_SRV" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "_imaps._tcp.pdostal.cz."
  type    = "SRV"
  ttl     = 300
  records = [
    "0 1 993 mail.messagingengine.com",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_pop3s_tcp_SRV" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "_pop3s._tcp.pdostal.cz."
  type    = "SRV"
  ttl     = 300
  records = [
    "10 1 995 mail.messagingengine.com",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_submission_tcp_SRV" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "_submission._tcp.pdostal.cz."
  type    = "SRV"
  ttl     = 300
  records = [
    "0 1 587 mail.messagingengine.com",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_home_A" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "home.pdostal.cz."
  type    = "A"
  ttl     = 300
  records = [
    "31.31.231.54",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_matomo_CNAME" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "matomo.pdostal.cz."
  type    = "CNAME"
  ttl     = 300
  records = [
    "witbier.pdostal.cz",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_witbier_A" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "witbier.pdostal.cz."
  type    = "A"
  ttl     = 300
  records = [
    "62.171.184.19",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_witbier_AAAA" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "witbier.pdostal.cz."
  type    = "AAAA"
  ttl     = 300
  records = [
    "2a02:c207:2046:2263::1",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_witbier_CNAME" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "*.witbier.pdostal.cz."
  type    = "CNAME"
  ttl     = 300
  records = [
    "witbier.pdostal.cz",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_www_CNAME" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "www.pdostal.cz."
  type    = "CNAME"
  ttl     = 300
  records = [
    "pdostal.cz",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_lychee_CNAME" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "lychee.pdostal.cz."
  type    = "CNAME"
  ttl     = 300
  records = [
    "witbier.pdostal.cz",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_miniflux_CNAME" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "miniflux.pdostal.cz."
  type    = "CNAME"
  ttl     = 300
  records = [
    "witbier.pdostal.cz",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_idm_CNAME" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "idm.pdostal.cz."
  type    = "CNAME"
  ttl     = 300
  records = [
    "witbier.pdostal.cz",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_coffeestout_A" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "coffeestout.pdostal.cz."
  type    = "A"
  ttl     = 300
  records = [
    "149.102.155.95",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_coffeestout_AAAA" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "coffeestout.pdostal.cz."
  type    = "AAAA"
  ttl     = 300
  records = [
    "2a02:c204:2214:8895::1",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_capitol_A" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "capitol.pdostal.cz."
  type    = "A"
  ttl     = 300
  records = [
    "147.93.130.115",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_capitol_AAAA" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "capitol.pdostal.cz."
  type    = "AAAA"
  ttl     = 300
  records = [
    "2605:a140:2228:4124::1",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_tata_A" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "tata.pdostal.cz."
  type    = "A"
  ttl     = 300
  records = [
    "31.31.237.34",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_auth_NS" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "auth.pdostal.cz."
  type    = "NS"
  ttl     = 300
  records = [
    "auth.pdostal.cz",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_auth_A" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "auth.pdostal.cz."
  type    = "A"
  ttl     = 300
  records = [
    "62.171.184.19",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_auth_AAAA" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "auth.pdostal.cz."
  type    = "AAAA"
  ttl     = 300
  records = [
    "2a02:c207:2046:2263::1",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_chalupavlasta_A" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "chalupavlasta.pdostal.cz."
  type    = "A"
  ttl     = 300
  records = [
    "86.61.128.138",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "pdostal_cz_chalupakadov_A" {
  zone_id = aws_route53_zone.pdostal_cz.zone_id
  name    = "chalupakadov.pdostal.cz."
  type    = "A"
  ttl     = 300
  records = [
    "86.61.128.134",
  ]
  allow_overwrite = true
}

# chalupa-vlasta.cz

resource "aws_route53_zone" "chalupa-vlasta_cz" {
  name = "chalupa-vlasta.cz"
}

resource "aws_route53_record" "chalupa-vlasta_cz_A" {
  zone_id = aws_route53_zone.chalupa-vlasta_cz.zone_id
  name    = "chalupa-vlasta.cz."
  type    = "A"
  ttl     = 300
  records = [
    "62.171.184.19",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "chalupa-vlasta_cz_AAAA" {
  zone_id = aws_route53_zone.chalupa-vlasta_cz.zone_id
  name    = "chalupa-vlasta.cz."
  type    = "AAAA"
  ttl     = 300
  records = [
    "2a02:c207:2046:2263::1",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "chalupa-vlasta_cz_www_CNAME" {
  zone_id = aws_route53_zone.chalupa-vlasta_cz.zone_id
  name    = "www.chalupa-vlasta.cz."
  type    = "CNAME"
  ttl     = 300
  records = [
    "chalupa-vlasta.cz",
  ]
  allow_overwrite = true
}


# chalupakadov.cz

resource "aws_route53_zone" "chalupakadov_cz" {
  name = "chalupakadov.cz"
}

resource "aws_route53_record" "chalupakadov_cz_A" {
  zone_id = aws_route53_zone.chalupakadov_cz.zone_id
  name    = "chalupakadov.cz."
  type    = "A"
  ttl     = 300
  records = [
    "62.171.184.19",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "chalupakadov_cz_AAAA" {
  zone_id = aws_route53_zone.chalupakadov_cz.zone_id
  name    = "chalupakadov.cz."
  type    = "AAAA"
  ttl     = 300
  records = [
    "2a02:c207:2046:2263::1",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "chalupakadov_cz_MX" {
  zone_id = aws_route53_zone.chalupakadov_cz.zone_id
  name    = "chalupakadov.cz."
  type    = "MX"
  ttl     = 300
  records = [
    "10 fe8124c360e04bd8.mx2.emailprofi.seznam.cz",
    "20 fe8124c360e04bd8.mx1.emailprofi.seznam.cz",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "chalupakadov_cz_www_CNAME" {
  zone_id = aws_route53_zone.chalupakadov_cz.zone_id
  name    = "www.chalupakadov.cz."
  type    = "CNAME"
  ttl     = 300
  records = [
    "chalupakadov.cz",
  ]
  allow_overwrite = true
}


# motorenovace.com

resource "aws_route53_zone" "motorenovace_com" {
  name = "motorenovace.com"
}

resource "aws_route53_record" "motorenovace_com_A" {
  zone_id = aws_route53_zone.motorenovace_com.zone_id
  name    = "motorenovace.com."
  type    = "A"
  ttl     = 300
  records = [
    "62.171.184.19",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "motorenovace_com_AAAA" {
  zone_id = aws_route53_zone.motorenovace_com.zone_id
  name    = "motorenovace.com."
  type    = "AAAA"
  ttl     = 300
  records = [
    "2a02:c207:2046:2263::1",
  ]
  allow_overwrite = true
}

resource "aws_route53_record" "motorenovace_com_www_CNAME" {
  zone_id = aws_route53_zone.motorenovace_com.zone_id
  name    = "www.motorenovace.com."
  type    = "CNAME"
  ttl     = 300
  records = [
    "motorenovace.com",
  ]
  allow_overwrite = true
}

